$( document ).ready(function() {
    let totalCnt = 0;
    let totalUn = 0;
    let totalBen = 0;
    let totalKo = 0;
    let totalTr = 0;
    let totalBez = 0;
    let totalMo = 0;
    let totalUs = 0;
    let totalPo = 0;
    let totalSti = 0;
    let totalSam = 0;

    let totalMaleCnt = 0;
    let totalMaleUn = 0;
    let totalMaleBen = 0;
    let totalMaleKo = 0;
    let totalMaleTr = 0;
    let totalMaleBez = 0;
    let totalMaleMo = 0;
    let totalMaleUs = 0;
    let totalMalePo = 0;
    let totalMaleSti = 0;
    let totalMaleSam = 0;

    let totalFemaleCnt = 0;
    let totalFemaleUn = 0;
    let totalFemaleBen = 0;
    let totalFemaleKo = 0;
    let totalFemaleTr = 0;
    let totalFemaleBez = 0;
    let totalFemaleMo = 0;
    let totalFemaleUs = 0;
    let totalFemalePo = 0;
    let totalFemaleSti = 0;
    let totalFemaleSam = 0;

    let classValues = [];
    let fieldValues = [];

    let usedIds = [];



    jQuery.get('hodnoty.csv', function(data) {
        const d = Papa.parse(data, {
            delimiter: ';'
        });

        for (let i = 0; i <= 1000; i++) {
            const data = d['data'][i];

            if (data[0] == null || data[0] === '') {
				break;
			}

            let id = data[3];
            if (id === "") {
                continue;
            }

            if (usedIds.indexOf(id) !== -1) {
                id = id + '_' + i;
            }

            usedIds.push(id);

            const un = data[28].replace(',', '.');
            const ben = data[29].replace(',', '.');
            const ko = data[30].replace(',', '.');
            const tr = data[31].replace(',', '.');
            const bez = data[32].replace(',', '.');
            const mo = data[33].replace(',', '.');
            const us = data[34].replace(',', '.');
            const po = data[35].replace(',', '.');
            const sti = data[36].replace(',', '.');
            const sam = data[37].replace(',', '.');

            const gender = data[5];
            const class_m = data[0];

            totalCnt++;
            totalUn += Number(un);
            totalBen += Number(ben);
            totalKo += Number(ko);
            totalTr += Number(tr);
            totalBez += Number(bez);
            totalMo += Number(mo);
            totalUs += Number(us);
            totalPo += Number(po);
            totalSti += Number(sti);
            totalSam += Number(sam);

            if (gender === "muž") {
                totalMaleCnt++;
                totalMaleUn += Number(un);
                totalMaleBen += Number(ben);
                totalMaleKo += Number(ko);
                totalMaleTr += Number(tr);
                totalMaleBez += Number(bez);
                totalMaleMo += Number(mo);
                totalMaleUs += Number(us);
                totalMalePo += Number(po);
                totalMaleSti += Number(sti);
                totalMaleSam += Number(sam);
            } else {
                totalFemaleCnt++;
                totalFemaleUn += Number(un);
                totalFemaleBen += Number(ben);
                totalFemaleKo += Number(ko);
                totalFemaleTr += Number(tr);
                totalFemaleBez += Number(bez);
                totalFemaleMo += Number(mo);
                totalFemaleUs += Number(us);
                totalFemalePo += Number(po);
                totalFemaleSti += Number(sti);
                totalFemaleSam += Number(sam);
            }

            if (classValues[class_m] == null) {
                classValues[class_m] = [];
                classValues[class_m]['cnt'] = 0;
                classValues[class_m]['un']=0;
                classValues[class_m]['ben']=0;
                classValues[class_m]['ko']=0;
                classValues[class_m]['tr']=0;
                classValues[class_m]['bez']=0;
                classValues[class_m]['mo']=0;
                classValues[class_m]['us']=0;
                classValues[class_m]['po']=0;
                classValues[class_m]['sti']=0;
                classValues[class_m]['sam']=0;
            }
            classValues[class_m]['cnt']++;
            classValues[class_m]['un']+=Number(un);
            classValues[class_m]['ben']+=Number(ben);
            classValues[class_m]['ko']+=Number(ko);
            classValues[class_m]['tr']+=Number(tr);
            classValues[class_m]['bez']+=Number(bez);
            classValues[class_m]['mo']+=Number(mo);
            classValues[class_m]['us']+=Number(us);
            classValues[class_m]['po']+=Number(po);
            classValues[class_m]['sti']+=Number(sti);
            classValues[class_m]['sam']+=Number(sam);

            const field = class_m.split(/[0-9]/)[0];

            if (fieldValues[field] == null) {
                fieldValues[field] = [];
                fieldValues[field]['cnt'] = 0;
                fieldValues[field]['un']=0;
                fieldValues[field]['ben']=0;
                fieldValues[field]['ko']=0;
                fieldValues[field]['tr']=0;
                fieldValues[field]['bez']=0;
                fieldValues[field]['mo']=0;
                fieldValues[field]['us']=0;
                fieldValues[field]['po']=0;
                fieldValues[field]['sti']=0;
                fieldValues[field]['sam']=0;
            }
            fieldValues[field]['cnt']++;
            fieldValues[field]['un']+=Number(un);
            fieldValues[field]['ben']+=Number(ben);
            fieldValues[field]['ko']+=Number(ko);
            fieldValues[field]['tr']+=Number(tr);
            fieldValues[field]['bez']+=Number(bez);
            fieldValues[field]['mo']+=Number(mo);
            fieldValues[field]['us']+=Number(us);
            fieldValues[field]['po']+=Number(po);
            fieldValues[field]['sti']+=Number(sti);
            fieldValues[field]['sam']+=Number(sam);

            const wrapper = $('#stats-wrapper');
            wrapper.append("<div class=\"stat-card\">\n" +
                "            <h1>" + id + "</h1>\n" +
                "<p>universalismus: " + un + ", benevolence: " + ben + ", konformismus: " + ko + ", tradice: " + tr + ", " +
                "bezpečnost: " + bez + ", moc: " + mo + ", úspěch: " + us + ", požitkářství: " + po + ", stimulace: " + sti + ", samostatnost:  " + sam + "</p>" +
                "            <canvas id='" + id.replace("(", "").replace(")", "").replace("/", "").replace("?", "") + "'></canvas>\n" +
                "        </div>");

            const ctx = document.getElementById(id.replace("(", "").replace(")", "")
                .replace("/", "").replace("?", "")).getContext('2d');

            const chartData = {
                labels: ['universalismus', 'benevolence', 'konformismus', 'tradice', 'bezpečnost', 'moc', 'úspěch', 'požitkářství', 'stimulace', 'samostatnost'],
                datasets: [{
                    backgroundColor: "rgba(0,0,200,0.2)",
                    data: [un, ben, ko, tr, bez, mo, us, po, sti, sam],
                }],
                displayLabels: false
            };
            const radarChart = new Chart(ctx, {
                type: 'radar',
                data: chartData,
                options: {
                    legend: {
                        display: false
                    },
                    scale: {
                        ticks: {
                            beginAtZero: false,
                            min: 1,
                            max: 6,
                            reverse: true
                        }
                    }
                }
            });
        }

        // Whole school chart
        const wsWrapper = $('#whole-school');
        wsWrapper.append("<div class=\"stat-card\">\n" +
            "            <h1>" + "Celá škola" + "</h1>\n" +
            "<p>universalismus: " + (totalUn/totalCnt).toFixed(2) +
            "<br> benevolence: " + (totalBen/totalCnt).toFixed(2) + "<br>" +
            "konformismus: " + (totalKo/totalCnt).toFixed(2) +
            "<br> tradice: " + (totalTr/totalCnt).toFixed(2) + "<br> " +
            "bezpečnost: " + (totalBez/totalCnt).toFixed(2) +
            "<br> moc: " + (totalMo/totalCnt).toFixed(2) + "<br>" +
            "úspěch: " + (totalUs/totalCnt).toFixed(2) + "<br>" +
            "požitkářství: " + (totalPo/totalCnt).toFixed(2) + "<br>" +
            "stimulace: " + (totalSti/totalCnt).toFixed(2)
            + "<br> samostatnost:  " + (totalSam/totalCnt).toFixed(2) + "</p>" +
            "            <canvas style='margin-top: 100px' id='ws-canvas'></canvas>\n" +
            "        </div>");

        const wsCtx = document.getElementById("ws-canvas").getContext('2d');

        const wsChartData = {
            labels: ['universalismus', 'benevolence', 'konformismus', 'tradice', 'bezpečnost', 'moc', 'úspěch', 'požitkářství', 'stimulace', 'samostatnost'],
            datasets: [{
                backgroundColor: "rgba(0,0,200,0.2)",
                data: [totalUn/totalCnt, totalBen/totalCnt, totalKo/totalCnt, totalTr/totalCnt, totalBez/totalCnt,
                    totalMo/totalCnt, totalUs/totalCnt, totalPo/totalCnt, totalSti/totalCnt, totalSam/totalCnt],
            }],
            displayLabels: false
        };
        const wsRadarChart = new Chart(wsCtx, {
            type: 'radar',
            data: wsChartData,
            options: {
                legend: {
                    display: false
                },
                scale: {
                    ticks: {
                        beginAtZero: false,
                        min: 1,
                        max: 6,
                        reverse: true
                    }
                }
            }
        });

        // CR
        const crWrapper = $('#whole-cr');
        jQuery.get('cr.csv', function(crDataCsv) {
            const dcr = Papa.parse(crDataCsv, {
                delimiter: ';'
            });
            const crData = dcr['data'][0];
            console.log(crData);

            const unCr = crData[28].replace(',', '.');
            const benCr = crData[29].replace(',', '.');
            const koCr = crData[30].replace(',', '.');
            const trCr = crData[31].replace(',', '.');
            const bezCr = crData[32].replace(',', '.');
            const moCr = crData[33].replace(',', '.');
            const usCr = crData[34].replace(',', '.');
            const poCr = crData[35].replace(',', '.');
            const stiCr = crData[36].replace(',', '.');
            const samCr = crData[37].replace(',', '.');
            crWrapper.append("<div class=\"stat-card\">\n" +
                "            <h1>" + "Celá ČR" + "</h1>\n" +
                "<p>universalismus: " + unCr +
                "<br> benevolence: " + benCr + "<br>" +
                "konformismus: " + koCr +
                "<br> tradice: " + trCr + "<br> " +
                "bezpečnost: " + bezCr +
                "<br> moc: " + moCr + "<br>" +
                "úspěch: " + usCr + "<br>" +
                "požitkářství: " + poCr + "<br>" +
                "stimulace: " + stiCr
                + "<br> samostatnost:  " + samCr + "</p>" +
                "            <canvas style='margin-top: 100px' id='cr-canvas'></canvas>\n" +
                "        </div>");

            const crCtx = document.getElementById("cr-canvas").getContext('2d');

            const crChartData = {
                labels: ['universalismus', 'benevolence', 'konformismus', 'tradice', 'bezpečnost', 'moc', 'úspěch', 'požitkářství', 'stimulace', 'samostatnost'],
                datasets: [{
                    backgroundColor: "rgba(0,0,200,0.2)",
                    data: [unCr, benCr, koCr, trCr, bezCr, moCr, usCr, poCr, stiCr, samCr],
                }],
                displayLabels: false
            };
            const crRadarChart = new Chart(crCtx, {
                type: 'radar',
                data: crChartData,
                options: {
                    legend: {
                        display: false
                    },
                    scale: {
                        ticks: {
                            beginAtZero: false,
                            min: 1,
                            max: 6,
                            reverse: true
                        }
                    }
                }
            });
        });

        // By gender
        const bgWrapper = $('#by-gender');
        // Male
        bgWrapper.append("<div class=\"stat-card\">\n" +
            "            <h1>" + "Muži" + "</h1>\n" +
            "<p>universalismus: " + (totalMaleUn/totalMaleCnt).toFixed(2) +
            "<br> benevolence: " + (totalMaleBen/totalMaleCnt).toFixed(2) + "<br>" +
            "konformismus: " + (totalMaleKo/totalMaleCnt).toFixed(2) +
            "<br> tradice: " + (totalMaleTr/totalMaleCnt).toFixed(2) + "<br> " +
            "bezpečnost: " + (totalMaleBez/totalMaleCnt).toFixed(2) +
            "<br> moc: " + (totalMaleMo/totalMaleCnt).toFixed(2) + "<br>" +
            "úspěch: " + (totalMaleUs/totalMaleCnt).toFixed(2) + "<br>" +
            "požitkářství: " + (totalMalePo/totalMaleCnt).toFixed(2) + "<br>" +
            "stimulace: " + (totalMaleSti/totalMaleCnt).toFixed(2)
            + "<br> samostatnost:  " + (totalMaleSam/totalMaleCnt).toFixed(2) + "</p>" +
            "            <canvas style='margin-top: 100px' id='bg-male-canvas'></canvas>\n" +
            "        </div>");

        // Female
        bgWrapper.append("<div class=\"stat-card\">\n" +
            "            <h1>" + "Ženy" + "</h1>\n" +
            "<p>universalismus: " + (totalFemaleUn/totalFemaleCnt).toFixed(2) +
            "<br> benevolence: " + (totalFemaleBen/totalFemaleCnt).toFixed(2) + "<br>" +
            "konformismus: " + (totalFemaleKo/totalFemaleCnt).toFixed(2) +
            "<br> tradice: " + (totalFemaleTr/totalFemaleCnt).toFixed(2) + "<br> " +
            "bezpečnost: " + (totalFemaleBez/totalFemaleCnt).toFixed(2) +
            "<br> moc: " + (totalFemaleMo/totalFemaleCnt).toFixed(2) + "<br>" +
            "úspěch: " + (totalFemaleUs/totalFemaleCnt).toFixed(2) + "<br>" +
            "požitkářství: " + (totalFemalePo/totalFemaleCnt).toFixed(2) + "<br>" +
            "stimulace: " + (totalFemaleSti/totalFemaleCnt).toFixed(2)
            + "<br> samostatnost:  " + (totalFemaleSam/totalFemaleCnt).toFixed(2) + "</p>" +
            "            <canvas style='margin-top: 100px' id='bg-female-canvas'></canvas>\n" +
            "        </div>");

        const bgMaleCtx = document.getElementById("bg-male-canvas").getContext('2d');
        const bgFemaleCtx = document.getElementById("bg-female-canvas").getContext('2d');

        const bgMaleChartData = {
            labels: ['universalismus', 'benevolence', 'konformismus', 'tradice', 'bezpečnost', 'moc', 'úspěch', 'požitkářství', 'stimulace', 'samostatnost'],
            datasets: [{
                backgroundColor: "rgba(0,0,200,0.2)",
                data: [totalMaleUn/totalMaleCnt, totalMaleBen/totalMaleCnt, totalMaleKo/totalMaleCnt, totalMaleTr/totalMaleCnt, totalMaleBez/totalMaleCnt,
                    totalMaleMo/totalMaleCnt, totalMaleUs/totalMaleCnt, totalMalePo/totalMaleCnt, totalMaleSti/totalMaleCnt, totalMaleSam/totalMaleCnt],
            }],
            displayLabels: false
        };

        const bgFemaleChartData = {
            labels: ['universalismus', 'benevolence', 'konformismus', 'tradice', 'bezpečnost', 'moc', 'úspěch', 'požitkářství', 'stimulace', 'samostatnost'],
            datasets: [{
                backgroundColor: "rgba(0,0,200,0.2)",
                data: [totalFemaleUn/totalFemaleCnt, totalFemaleBen/totalFemaleCnt, totalFemaleKo/totalFemaleCnt, totalFemaleTr/totalFemaleCnt, totalFemaleBez/totalFemaleCnt,
                    totalFemaleMo/totalFemaleCnt, totalFemaleUs/totalFemaleCnt, totalFemalePo/totalFemaleCnt, totalFemaleSti/totalFemaleCnt, totalFemaleSam/totalFemaleCnt],
            }],
            displayLabels: false
        };

        const bgMaleRadarChart = new Chart(bgMaleCtx, {
            type: 'radar',
            data: bgMaleChartData,
            options: {
                legend: {
                    display: false
                },
                scale: {
                    ticks: {
                        beginAtZero: false,
                        min: 1,
                        max: 6,
                        reverse: true
                    }
                }
            }
        });

        const bgFemaleRadarChart = new Chart(bgFemaleCtx, {
            type: 'radar',
            data: bgFemaleChartData,
            options: {
                legend: {
                    display: false
                },
                scale: {
                    ticks: {
                        beginAtZero: false,
                        min: 1,
                        max: 6,
                        reverse: true
                    }
                }
            }
        });

        // By class
        for (const class_m in classValues) {
            const bcWrapper = $('#by-class');
            bcWrapper.append("<div class=\"stat-card\">\n" +
                "            <h1>" + class_m.toString() + "</h1>\n" +
                "<p>universalismus: " + (classValues[class_m]['un']/classValues[class_m]['cnt']).toFixed(2) +
                "<br> benevolence: " + (classValues[class_m]['ben']/classValues[class_m]['cnt']).toFixed(2) + "<br>" +
                "konformismus: " + (classValues[class_m]['ko']/classValues[class_m]['cnt']).toFixed(2) +
                "<br> tradice: " + (classValues[class_m]['tr']/classValues[class_m]['cnt']).toFixed(2) + "<br> " +
                "bezpečnost: " + (classValues[class_m]['bez']/classValues[class_m]['cnt']).toFixed(2) +
                "<br> moc: " + (classValues[class_m]['mo']/classValues[class_m]['cnt']).toFixed(2) + "<br>" +
                "úspěch: " + (classValues[class_m]['us']/classValues[class_m]['cnt']).toFixed(2) + "<br>" +
                "požitkářství: " + (classValues[class_m]['po']/classValues[class_m]['cnt']).toFixed(2) + "<br>" +
                "stimulace: " + (classValues[class_m]['sti']/classValues[class_m]['cnt']).toFixed(2)
                + "<br> samostatnost:  " + (classValues[class_m]['sam']/classValues[class_m]['cnt']).toFixed(2) + "</p>" +
                "            <canvas style='margin-top: 100px' id='bc-canvas" + class_m + "'></canvas>\n" +
                "        </div>");

            const bcCtx = document.getElementById("bc-canvas" + class_m).getContext('2d');

            const bcChartData = {
                labels: ['universalismus', 'benevolence', 'konformismus', 'tradice', 'bezpečnost', 'moc', 'úspěch', 'požitkářství', 'stimulace', 'samostatnost'],
                datasets: [{
                    backgroundColor: "rgba(0,0,200,0.2)",
                    data: [classValues[class_m]["un"]/classValues[class_m]['cnt'], classValues[class_m]["ben"]/classValues[class_m]['cnt'], classValues[class_m]["ko"]/classValues[class_m]['cnt'], classValues[class_m]["tr"]/classValues[class_m]['cnt'], classValues[class_m]["bez"]/classValues[class_m]['cnt'],
                        classValues[class_m]["mo"]/classValues[class_m]['cnt'], classValues[class_m]["us"]/classValues[class_m]['cnt'], classValues[class_m]["po"]/classValues[class_m]['cnt'], classValues[class_m]["sti"]/classValues[class_m]['cnt'], classValues[class_m]["sam"]/classValues[class_m]['cnt']],
                }],
                displayLabels: false
            };

            const bcRadarChart = new Chart(bcCtx, {
                type: 'radar',
                data: bcChartData,
                options: {
                    legend: {
                        display: false
                    },
                    scale: {
                        ticks: {
                            beginAtZero: false,
                            min: 1,
                            max: 6,
                            reverse: true
                        }
                    }
                }
            });
        }

        // By field
        for (const field in fieldValues) {
            const bfWrapper = $('#by-field');
            bfWrapper.append("<div class=\"stat-card\">\n" +
                "            <h1>" + field.toString() + "</h1>\n" +
                "<p>universalismus: " + (fieldValues[field]['un']/fieldValues[field]['cnt']).toFixed(2) +
                "<br> benevolence: " + (fieldValues[field]['ben']/fieldValues[field]['cnt']).toFixed(2) + "<br>" +
                "konformismus: " + (fieldValues[field]['ko']/fieldValues[field]['cnt']).toFixed(2) +
                "<br> tradice: " + (fieldValues[field]['tr']/fieldValues[field]['cnt']).toFixed(2) + "<br> " +
                "bezpečnost: " + (fieldValues[field]['bez']/fieldValues[field]['cnt']).toFixed(2) +
                "<br> moc: " + (fieldValues[field]['mo']/fieldValues[field]['cnt']).toFixed(2) + "<br>" +
                "úspěch: " + (fieldValues[field]['us']/fieldValues[field]['cnt']).toFixed(2) + "<br>" +
                "požitkářství: " + (fieldValues[field]['po']/fieldValues[field]['cnt']).toFixed(2) + "<br>" +
                "stimulace: " + (fieldValues[field]['sti']/fieldValues[field]['cnt']).toFixed(2)
                + "<br> samostatnost:  " + (fieldValues[field]['sam']/fieldValues[field]['cnt']).toFixed(2) + "</p>" +
                "            <canvas style='margin-top: 100px' id='bf-canvas" + field + "'></canvas>\n" +
                "        </div>");

            const bfCtx = document.getElementById("bf-canvas" + field).getContext('2d');

            const bfChartData = {
                labels: ['universalismus', 'benevolence', 'konformismus', 'tradice', 'bezpečnost', 'moc', 'úspěch', 'požitkářství', 'stimulace', 'samostatnost'],
                datasets: [{
                    backgroundColor: "rgba(0,0,200,0.2)",
                    data: [fieldValues[field]["un"]/fieldValues[field]['cnt'], fieldValues[field]["ben"]/fieldValues[field]['cnt'], fieldValues[field]["ko"]/fieldValues[field]['cnt'], fieldValues[field]["tr"]/fieldValues[field]['cnt'], fieldValues[field]["bez"]/fieldValues[field]['cnt'],
                        fieldValues[field]["mo"]/fieldValues[field]['cnt'], fieldValues[field]["us"]/fieldValues[field]['cnt'], fieldValues[field]["po"]/fieldValues[field]['cnt'], fieldValues[field]["sti"]/fieldValues[field]['cnt'], fieldValues[field]["sam"]/fieldValues[field]['cnt']],
                }],
                displayLabels: false
            };

            const bfRadarChart = new Chart(bfCtx, {
                type: 'radar',
                data: bfChartData,
                options: {
                    legend: {
                        display: false
                    },
                    scale: {
                        ticks: {
                            beginAtZero: false,
                            min: 1,
                            max: 6,
                            reverse: true
                        }
                    }
                }
            });
        }
    });
});
