# Schwarz
Schwarz je program pro vyhodnocení výsledků osobnostních dotazníků a zobrazení jejich výsledků pomocí grafů.
## Návod k použití
- vytvořte si kopii následující tabulky: https://drive.google.com/file/d/1dxBoWI_9gzD1gIjIIIhzEe84cvIPj74a/view?usp=sharing
- vyplňte vaši tabulku pro vyhodnocení výsledků podle vzorového řádku
- smažte záhlaví i zápatí tabulky tak, aby v ní zůstaly pouze řádky jednotlivých výsledků
- záznamy přesuňte nahoru tak, aby byl první záznam na prvním řádku tabulky
- stáhněte program jako ZIP
- extrahujte program do složky
- stáhněte tabulku jako soubor CSV (pro oddělení sloupců musí být použity středníky), přejmenujte soubor na hodnoty.csv a nahraďte tímto souborem soubor hodnoty.csv s ukázkovými daty, který již ve složce je
- aplikaci přesuňte na server a nakonfigurujte webový server
- na serveru spusťte ve složce aplikace příkaz *npm install* pro nainstalování závislostí aplikace
- přejděte na URL adresu nakonfigurovanou na webovém serveru a prohlížejte statistiky
