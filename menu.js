$(document).ready(() => {
    const ws = $("#whole-school");
    const wsm = $("#whole-school-menu");

    const bg = $("#by-gender");
    const bgm = $("#by-gender-menu");

    const bc = $("#by-class");
    const bcm = $("#by-class-menu");

    const bf = $("#by-field");
    const bfm = $("#by-field-menu");

    const bi = $("#stats-wrapper");
    const bim = $("#stats-wrapper-menu");

    const cr = $("#whole-cr");
    const crm = $("#whole-cr-menu");

    wsm.on('click', function () {
        hideAll();
        ws.removeClass('gone');
        wsm.addClass('active');
    });

    bgm.on('click', function () {
        hideAll();
        bg.removeClass('gone');
        bgm.addClass('active');
    });

    bcm.on('click', function () {
        hideAll();
        bc.removeClass('gone');
        bcm.addClass('active');
    });

    bfm.on('click', function () {
        hideAll();
        bf.removeClass('gone');
        bfm.addClass('active');
    });

    bim.on('click', function () {
        hideAll();
        bi.removeClass('gone');
        bim.addClass('active');
    });

    crm.on('click', function () {
        hideAll();
        cr.removeClass('gone');
        crm.addClass('active');
    });

    function hideAll() {
        ws.addClass('gone');
        bg.addClass('gone');
        bc.addClass('gone');
        bf.addClass('gone');
        bi.addClass('gone');
        cr.addClass('gone');

        wsm.removeClass('active');
        bgm.removeClass('active');
        bcm.removeClass('active');
        bfm.removeClass('active');
        bim.removeClass('active');
        crm.removeClass('active');
    }

    hideAll();
    ws.removeClass('gone');
    wsm.addClass('active');
});
